//
//  main.m
//  SO 27883313
//
//  Created by Abizer Nasir on 11/01/2015.
//  Copyright (c) 2015 Jungle Candy Software Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  TableViewController.m
//  SO 27883313
//
//  Created by Abizer Nasir on 11/01/2015.
//  Copyright (c) 2015 Jungle Candy Software Ltd. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@property (strong, nonatomic) NSArray *classCodes;
@property (strong, nonatomic) NSArray *classNames;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupModel];
    // Quick way of not letting content appear under the status bar at the start.
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.classCodes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * const CellIdentifier = @"Class";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    NSUInteger row = indexPath.row;
    cell.textLabel.text = self.classCodes[row];
    cell.detailTextLabel.text = self.classNames[row];
    
    return cell;
}

#pragma Private methods

- (void)setupModel {
    _classCodes = @[@"CHEM 1100",
                    @"CHEM 2100 ",
                    @"CHEM 3415W",
                    @"CHEM 3511",
                    @"CHEM 3521",
                    @"CHEM 4600",
                    @"PHYS 1150",
                    @"PHYS 2150",
                    @"MATH 1201",
                    @"MATH 1206",
                    @"Adv courses",
                    @"CHEM 2700"];
    
    _classNames = @[@"General Chemistry I",
                    @"General Chemistry II",
                    @"Analytical Chemistry",
                    @"Organic Chemistry I",
                    @"Organic Chemistry II",
                    @"Physical Chemistry(HP)",
                    @"General Physics I",
                    @"General Physics II",
                    @"Calculus I",
                    @"Calculus II",
                    @"pick 5 creit",
                    @"Introduction to Inorganic Chemistry"];
}

@end
